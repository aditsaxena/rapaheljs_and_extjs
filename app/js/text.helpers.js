function draw_text(canvas, w, h, text, font_size){
  if(typeof font_size === "undefined") { font_size = '8px'; }
  return canvas.text(w, h, text).attr('fill', '#333').attr('font-size', font_size);
}

function draw_text_label(w, h, text) {
  text = draw_text(canvDet, w + 20, h + 20, text, '12px');
  text.node.id = 'txt_hover_label';
  
  var box = text.getBBox();
  var rect = canvDet.rect(box.x - 3, box.y - 3, box.width + 6, box.height + 6).attr({ fill: '#fee', stroke: '' });
  
  rect.node.id = 'txt_hover_label_bg';
  text.toFront();
}

Raphael.el.hoverInBounds = function(inFunc, outFunc) {
  var inBounds = false;

  // Mouseover function. Only execute if `inBounds` is false.
  this.mouseover(function() {
    if (!inBounds) {
      inBounds = true;
      inFunc.call(this);
    }
  });

  // Mouseout function
  this.mouseout(function(e) {
    var x = e.offsetX || e.clientX,
        y = e.offsetY || e.clientY;
    
    // Return `false` if we're still inside the element's bounds
    if (this.isPointInside(x, y)) return false;
    
    inBounds = false;
    outFunc.call(this);
  });

  return this;
}

function circleHoverIn() {
  this.attr({ 'stroke': '#999', 'stroke-width': '6px'});
  draw_text_label(this.attr('cx'), this.attr('cy'), this.data('label-value'));
}

function circleHoverOut() {
  this.attr({ 'stroke-width': '0px'});
  Ext.get('txt_hover_label').remove();
  Ext.get('txt_hover_label_bg').remove();
}

