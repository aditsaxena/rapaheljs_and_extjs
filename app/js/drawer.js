function drawGrid(paper, relevances, temp){
  opts = {
    h_padding: 50,
    w_padding: 10,
    h_scale:   4,
    w_spacing: 50
  }
  
  canvas_w = paper.canvas.offsetWidth;
  canvas_h = paper.canvas.offsetHeight;
  
  function max_w_steps() {
    return Math.floor((canvas_w) / opts.w_spacing );
  }
  
  function w_pos(i) {
    return i * opts.w_spacing + opts.w_padding;
  }
  
  function h_pos(i) {
    return (canvas_h - opts.h_padding * 2) / opts.h_scale * i + opts.h_padding;
  }
  
  function draw_W_lines() {
    for (var i = 0; i <= opts.h_scale; i++) {
      var w = max_w_steps() * opts.w_spacing;
      paper.path( ["M", opts.w_padding, h_pos(i), "L", w + opts.w_padding, h_pos(i)] ).attr("stroke", "#ccc");
    };
  }
  
  
  function draw_H_lines() {
    for (var i = 0; i <= max_w_steps(); i++) {
      var w = i * opts.w_spacing + opts.w_padding;
      paper.path( ["M", w, opts.h_padding/2, "L", w, canvas_h - opts.h_padding/2 ] ).attr("stroke", "#ccc");
    };
  }
  
  function get_top_label(measure) {
    measure.t = new Date(measure.t);
    time = measure.t.getHours() + ":" + measure.t.getMinutes();
    return time;
  }
  
  function get_bottom_label(measure) {
    total_rel = measure.data.map(function (rel){
      return rel.size;
    }).reduce(function(total, el) { return total + el })
    
    return total_rel;
  }
  
  function get_circle_radius(rel) {
    return rel / 5;
  }
  
  function draw_measure(w_pos, measure) {
    draw_text(paper, w_pos, canvas_h - opts.h_padding / 2, get_top_label(measure));
    draw_text(paper, w_pos, opts.h_padding / 2, get_bottom_label(measure));
    
    for (var j = 0; j <= opts.h_scale; j++) {
      var radius = get_circle_radius(measure.data[j].size);
      circle = paper.circle(w_pos, h_pos(j), radius).attr({
        stroke: '#EEE',
        fill: relevances[4 - j].color
      });
      circle.hover(circleHoverIn, circleHoverOut).data('label-value', measure.data[j].size);
    };
  }
  
  function draw_measures(measures) {
    latest_measurements = measures.abacus_graph_data.slice(-max_w_steps() - 1);
    
    for (var imeasure = 0; imeasure <= Math.min(max_w_steps(), latest_measurements.length -1); imeasure++) {
      var w_pos = imeasure * opts.w_spacing + opts.w_padding;
      measure = latest_measurements[imeasure];
      draw_measure(w_pos, measure);
    };
  }
  
  function relevance_legend_vals (measures) {
    return measures.map(function (el) {
      return el.data.map(function (data) {
        return data.size;
      });
    }).reduce(function(prevMeasure, currMeasure, index, array){
      
      var finMeasure = [];
      for (var rel_code = 0; rel_code < prevMeasure.length; rel_code++) {
        finMeasure[rel_code] = prevMeasure[rel_code] + currMeasure[rel_code]
      };
      
      return finMeasure;
    });
  }
  
  if(temp == 'left') {
    draw_W_lines();
    
    percentage_of_row_height = 80;
    bar_thickness = canvas_h / opts.h_scale / 2 / 100 * percentage_of_row_height;
    
    // ----------------------------
    
    tmp_arr = relevance_legend_vals(measures.abacus_graph_data);
    tmp_arr_max = arrMax(tmp_arr);
    
    for (var irel_type = 0; irel_type < tmp_arr.length; irel_type++) {
      
      rel_code = (relevances.length - 1) - irel_type;
      
      cur_height = h_pos(rel_code);
      bar_width = tmp_arr[rel_code] * (canvas_w - opts.w_padding * 2) / tmp_arr_max;
      label = relevances[rel_code].name + " (" + tmp_arr[rel_code] + ")";
      
      bar = paper.rect(opts.w_padding, cur_height - bar_thickness/2, bar_width, bar_thickness).attr({
        'stroke': '',
        'fill': relevances[rel_code].color
      });
      draw_text(paper, opts.w_padding * 2, cur_height, label, '12px').attr({
        'font-weight': 'bold',
        'text-anchor': 'start',
        'fill': '#fff',
        'stroke': '#000',
        'stroke-width': 0.5
      })
      
      // ----------------------------
      
    };
    
  }else{
    draw_W_lines();
    draw_H_lines();
    draw_measures(measures);
  };
  
}

