function arrMax(arr) {
  return arr.reduce(function(prevValue, currValue, index, array){
    return Math.max(prevValue, currValue);
  });
}

