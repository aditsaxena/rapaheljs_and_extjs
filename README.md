JavaScript Plugin Development Test.

Working demo: http://jsfiddle.net/saxxib/c9naT/

## Glossary

  Platform
  A Single Page Application based on ExtJS4 framework.
  
  Plugin
  An external component implemented using libraries outside ExtJS4.
  
  Adapter
  A custom wrapper class interfacing the Plugin with the Platform.

## Abstract

The Plugin follows an event-driven pattern.  

It will communicate with the Platform only through its
Adapter. It will display an `Abacus graph`, using the `Raphael Library` (http://raphaeljs.com/ ).  

The candidate can eventually use also other libraries only if they are not conflicting with `ExtJS4 Framework` (es: `jQuery`) and may assume that such libraries are all loaded in the index.html and available at the Plugin scope. A list of these additional libraries will be produced by the candidate with his work.


As input, **the Plugin will receive a `JSON object` containing a list of dictionaries (named data and t=time)**: the date also is a dictionary, having two keys, relevance and size, and their relative values.
The Plugin shall continue polling the Adapter indefinitely for such data stream, at intervals defined during the initialization phase. Using the data in the JSON object, the Plugin will display a chart as output, similar to the picture below.

![Implementation example](https://dl.dropboxusercontent.com/u/46895700/tests/rapaheljs_and_extjs/doc/ref.png)

In details, on the right, **the graph will have time (each minute) on the x-assis, which will scroll to the left (shift) when a new set of data is received**. For each circle, its radius size will depend on the value received: the bigger the value, the bigger the circle. For each column, on top of it, the sum of the items per column will be displayed. **On the left side, instead, a legend (including the bar chart) will display also the sum of each instance for each item**, from the start_time to the end_time of the poll range (means the sum of all the currently displayed instants in the graph). All the changes of status (mouse clicks, focus, selection, ...) shall be reported to the Platform, calling the appropriate Adapter method. In particular, **when the mouse pointer reaches a circle, the size of its relevance will be displayed and the circle will be outlined** as in the picture above (similar behaviors are also acceptable, i.e.: http://raphaeljs.com/github/dots.html ). Also, when the user clicks on an item of the legend, the expected behavior is to toggle on/off these items on the chart, and, again, report the
event to the Adapter.


## Details


1. The Plugin receives its Adapter reference as a parameter of the Plugin.init method.
2. The Plugin can send events and data requests to the Platform. This communication with the Platform (and eventually with other plugins) is performed by the Plugin calling the method sendEvent on the Platform global object named Adapter;
3. The Plugin does not receive events directly, it is invoked only by the Adapter, who calls an event handling method of the Plugin, whose name matches the name of the event message being managed by the Adapter. A list of all the supported event messages must be exposed by the Plugin: the Adapter will retrieve this list calling Plugin.events during the initialization phase. These methods are the keys and values of Plugin.events. An handler is called when an event matching the name of the handler bubbles inside the system; the data attached to the event is put in the parameters variable of the event handler signature. For example, if the Adapter receives an event message named circle_select, and the Plugin is listening to it, the Adapter will call the event handler named Plugin.events.circle_select. An event name may contain only letters and underscores.
4. Also data queries (both requests and replies) will be handled as events. The Plugin sends data and requests calling the Adapter.sendEvent method and it will use the event messages named data_request and data_answer for them. For example, a data request would be issued as Adapter.sendEvent(“data_request”, <dict_args>), where args contains the context filter, the API id (read, list, save, count...). Only “read” API is applicable to this case.
5. Based on the bootstrap information, the Plugin will start polling the Adapter, sending data_request events each polling_interval. Each data request event message will contain also the updated polling range (start_time and end_time) in the parameters attribute of the sendEvent. At each new request, the polling range must be shifted of a polling_interval unit. The Adapter will reply to this request, calling the “data_answer” method of the Plugin, and passing the JSON object described in 3.3, as parameter of the method.
6. When an event matches a Plugin event handler name, the handler methods get called and receives the event_name (because the same function can be used as different handlers), the sender_id and the parameters attached to the event.
7. The Plugin will access the DOM and attaches its output to a <div> identified by an id equal to the plugin_id parameter, passed at initialization phase.
8. The candidate will use the following convention for names and colors to be used in the chart:
  
  | relevance | name     | color  |
  |-----------|----------|--------|
  | 4         | Severe   | d1333f |
  | 3         | High     | f69c35 |
  | 2         | Elevated | f5d711 |
  | 1         | Guarded  | 3473ae |
  | 0         | Low      | 199969 |
  
## Plugin Initialization Phase

During the initialization phase, the Platform: 

1. instantiates the Adapter as a global object;
2. instantiates and registers the Plugin calling the constructor and then its Plugin.init method,
passing to it the configuration parameters of the Plugin and a reference to the Adapter.


At this time, the Adapter instance will register all the events listened by the Plugin. This operation is done by cycling on plugin.events. structure.

Note that the Platform shall be able to reuse the Plugin.init method also to reinitialize the plugin when the time range and polling interval, or other parameters have been changed by the user.

## Plugin Interface

The following keys must be implemented on the Plugin:


**init:** function(bootstrap, language, adapter) Description
it is used by the platform to instantiate a plugin.

Parameters  

• bootstrap is the component ui instance relative to the wrapped component.

    This object will have the following attributes:  
    
    • plugin_id = &lt;string&gt; // the &lt;div&gt; id where to append the plugin output
    • start_time = &lt;datetime&gt;
    • end_time = &lt;datetime&gt;
    • polling_interval = &lt;int&gt; // number of seconds
    • language is a dictionary for strings localization;
    • adapter is a reference to its adapter;

events: 
    
    {
       eventName1: function(eventType, sender_id, paramenters),
       ...
    }

**Description**  

An object containing event handlers. The whole set of event handlers contains the logic of the component.

**Parameters**  

• eventName1 is an event name for both the event message and the handler;  
• eventType is the event structure; useful if more event types are handled by the very same function;  
• sender_id is the id of the component/plugin that raised the event;  
• parameters (optional) is the object containing additional data associated to the event.  

## The Adapter class for the Plugin
The Adapter object only exposes the following method to the Plugin: sendEvent: function(eventType, parameters)

**Description**

This method is used by the Plugin to send an event/request to the Adapter: the difference between event and request is purely semantic and responsibility of the adapter; depending on the type of the event, the adapter may satisfy the request or bubble the event up to the Platform infrastructure.

### Parameters

• **eventType:** a string describing the event;  
• **parameters:** (optional) additional data associated to the event, passed as an object.

## An instance example of the JSON object passed to the Plugin

    {
        "abacus_graph_data": [
            {
                "data": [
                    { "relevance": 0, "size":  0 },
                    { "relevance": 1, "size":  0 },
                    { "relevance": 2, "size": 73 },
                    { "relevance": 3, "size":  0 },
                    { "relevance": 4, "size": 43 }
                ],
                "t": "2012-06-08 12:44:00+02:00"
            },{
                .....
            },{
                .....
            },{
                "data": [
                    { "relevance": 0, "size":   0 },
                    { "relevance": 1, "size":   0 },
                    { "relevance": 2, "size":  73 },
                    { "relevance": 3, "size":   0 },
                    { "relevance": 4, "size": 101 }
                ],
                "t": "2012-06-08 12:58:00+02:00"
            }
        ]
    }



